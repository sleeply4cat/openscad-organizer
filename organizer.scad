$fn=25;

thiskness = 4.15;
notch_step = 25;
undercut_size=3.3/2;

full_x=240;
full_y=200;
full_z=120;

front_z=40;

center_plate_offset=49;

back_dividers_count=4;


noice_offset=0.01;


module notch(count=0, length=0, offset=0, undercut=0){
    translate([offset - noice_offset*2, -noice_offset*2, -noice_offset*2])
    for(i = [0 : count])
        translate([i * length * 2, 0, 0]) 
        {
            cube([length + noice_offset*4, thiskness + noice_offset*4, thiskness + noice_offset*4]);
            translate([undercut, thiskness, noice_offset])
                cylinder(thiskness + noice_offset*2, undercut, undercut, $fn=10);
            translate([length - undercut, thiskness, noice_offset])
                cylinder(thiskness + noice_offset*2, undercut, undercut, $fn=10);
        }
}

module notch_inner(count=0, length=0, offset=0, undercut=0){
    translate([offset - noice_offset*2, -noice_offset*2, -noice_offset])
    for(i = [0 : count])
        translate([i * length * 2, 0, 0]) 
        {
            cube([length + noice_offset*4, thiskness + noice_offset*2, thiskness + noice_offset*2]);
            
            translate([undercut, thiskness, 0])
                cylinder(thiskness + noice_offset*4, undercut, undercut, $fn=10);
            translate([length - undercut, thiskness, 0])
                cylinder(thiskness + noice_offset*4, undercut, undercut, $fn=10);
            
            translate([undercut, 0, 0])
                cylinder(thiskness + noice_offset*4, undercut, undercut, $fn=10);
            translate([length - undercut, 0, 0])
                cylinder(thiskness + noice_offset*4, undercut, undercut, $fn=10);
        }
}

module notched_plate(width, height, disable, offset=[0,0,0,0]){
    difference()
    {
        translate([-noice_offset, -noice_offset, 0])
            cube([width + noice_offset * 2, height + noice_offset * 2, thiskness]);
        
        if (!disable[0]) notch(width/notch_step, notch_step, offset[0], undercut_size);
        
        if (!disable[1]) translate([0, height, 0])
        mirror([0,1,0])
        notch(width/notch_step, notch_step, offset[1], undercut_size);
        
        if (!disable[2]) rotate([0,0,90])
        mirror([0,1,0])
        notch(height/notch_step / 2, notch_step, offset[2], undercut_size);
        
        if (!disable[3])translate([width, 0, 0])
        rotate([0,0,90])
        notch(height/notch_step / 2, notch_step, offset[3], undercut_size);
    }
}






module rounder(radius){
    translate([noice_offset/2, noice_offset/2, -noice_offset/2])
    linear_extrude(thiskness + noice_offset)
    difference()
    {
        square(radius + noice_offset);
        circle(radius + noice_offset);
    }
}


module bottom(){
    difference()
    {
        notched_plate(full_x, full_y);
        translate([0, center_plate_offset, 0])
            mirror([0,1,0])
            notch_inner(full_x / (notch_step * 2), notch_step, notch_step, undercut_size);
        
        for(i = [1 : back_dividers_count])
            translate([ i * full_x / (back_dividers_count + 1) + thiskness, -notch_step, 0])
            rotate([0,0,90])
            notch_inner(center_plate_offset / (notch_step * 2), notch_step, notch_step, undercut_size);
        
        translate([full_x/2 + thiskness, center_plate_offset - thiskness])
            rotate([0,0,90])
            notch_inner((full_y - center_plate_offset) / (notch_step * 2) - 1, notch_step, notch_step, undercut_size);
        
        translate([full_x / 2, (full_y - center_plate_offset) / 2 + center_plate_offset, 0])
            mirror([0,1,0])
            notch_inner(full_x /2 / (notch_step * 2) - 1, notch_step, notch_step, undercut_size);
        
        translate([full_x/4 * 3 + thiskness, center_plate_offset + (full_y-center_plate_offset) / 2 - thiskness])
            rotate([0,0,90])
            notch_inner((full_y - center_plate_offset) / 2 / (notch_step * 2), notch_step, notch_step, undercut_size);
    }
}

module back(){
    difference()
    {
        notched_plate(full_x, full_z, disable=[0,1,0,0], offset=[notch_step,0,0,0]);
        
        for(i = [1 : back_dividers_count])
            translate([ i * full_x / (back_dividers_count + 1) + thiskness, 0, 0])
            rotate([0,0,90])
            notch_inner(full_z / (notch_step * 2) - 1, notch_step, notch_step, undercut_size);
    }
}


module backplate(){
    rounder_radius=full_z - front_z;
    difference()
    {
        notched_plate(full_z, full_y, disable=[0,1,0,1], offset=[notch_step,0,notch_step,0]);
           
        translate([full_z - rounder_radius, full_y - rounder_radius, 0])
            rounder(rounder_radius);
    }
}


module back_left(){
    difference()
    {
        backplate();
        
        translate([0, full_y, 0])
            mirror([0,1,0])
            notch(0, notch_step, 0, undercut_size);
        
        translate([0, center_plate_offset, 0])
            mirror([0,1,0])
            notch_inner(full_z / (notch_step * 2) - 1, notch_step, notch_step, undercut_size);
    }
}
module back_right(){
    difference()
    {
        backplate();
        
        translate([0, full_y, 0])
            mirror([0,1,0])
            notch(0, notch_step, 0, undercut_size);
        
        translate([0, center_plate_offset, 0])
            mirror([0,1,0])
            notch_inner(full_z / (notch_step * 2) - 1, notch_step, notch_step, undercut_size);
        
        translate([0, (full_y - center_plate_offset) / 2 + center_plate_offset, 0])
            mirror([0,1,0])
            notch_inner(full_z/2 / (notch_step * 2) - 1, notch_step, notch_step, undercut_size);
    }
}

module front(){
    difference()
    {
        rounder_size=front_z/4;
        notched_plate(full_x, front_z,thiskness, disable=[0,1,0,0], offset=[notch_step,0,notch_step,notch_step]);
        
        translate([full_x/2 + front_z/2 - rounder_size,front_z/2 + rounder_size,0])
            rotate([0,0,90])
            rounder(rounder_size);
        
        difference()
        {
            translate([0 - noice_offset*2, front_z/2 + noice_offset , -noice_offset])
                cube([full_x/2 + noice_offset*2, front_z/2 + noice_offset, thiskness + noice_offset*2]);
            translate([full_x/2 - rounder_size,front_z/2 + rounder_size,0])
                rotate([0,0,270])
                rounder(rounder_size);
        }
        
        translate([full_x / 4 * 3 + thiskness, 0, 0])
            rotate([0,0,90])
            notch_inner(full_z/2 / (notch_step * 2) - 1, notch_step, 0, undercut_size);
    }
}

module center_plate(){
    difference()
    {
        notched_plate(full_x, full_z, thiskness, disable=[0,1,0,0]);
        
        translate([full_x/2 + thiskness, 0, 0])
            rotate([0,0,90])
            notch_inner(full_z / (notch_step * 2) - 1, notch_step, notch_step, undercut_size);
        
        for(i = [1 : back_dividers_count])
            translate([ i * full_x / (back_dividers_count + 1) + thiskness, 0, 0])
            rotate([0,0,90])
            notch_inner(full_z / (notch_step * 2) - 1, notch_step, notch_step, undercut_size);
    }
}

module divider_center(){
    rounder_radius=full_z - front_z;

    difference()
    {
        notched_plate(full_z, full_y - center_plate_offset, thiskness, disable=[0, 1, 0, 1]);
        
        translate([full_z - rounder_radius , full_y - center_plate_offset - rounder_radius, 0])
            rounder(rounder_radius);

        
        translate([0, (full_y - center_plate_offset) / 2 + thiskness, 0])
            mirror([0,1,0])
            notch_inner(full_z/2 / (notch_step * 2) - 1, notch_step, notch_step, undercut_size);
    }
    
}

module divider_back(){
    notched_plate(center_plate_offset, full_z, thiskness, disable=[0,1,0,0], offset=[notch_step,0,0,0]);
}

module divider_left(){
    difference()
    {
        notched_plate(full_x/2, full_z/2, thiskness, disable=[0,1,0,0]);
        
        translate([full_x / 4 + thiskness, -2 * thiskness, 0])
            rotate([0,0,90])
            notch_inner(full_z/2 / (notch_step * 2) - 1, notch_step, notch_step, undercut_size);
    }
}

module separator_left(){
    notched_plate(full_x/2 - center_plate_offset + 2 * thiskness, front_z, thiskness, disable=[0,1,0,0], offset=[0,0,0 - thiskness, notch_step]);
}


module assembly(){

    color("yellow")
        bottom();
    
    color ("red")
    translate([0,thiskness,0])
    rotate([90,0,0])
        back();
    
    color("blue")
    translate([full_x,0,0])
    rotate([0,270,0])
        back_right();
    
    color("purple")
    translate([thiskness,0,0])
    rotate([0,270,0])
        back_left();
    
    color("green")
        translate([0, full_y, 0])
        rotate([90,0,0])
        front();
        
    color("cyan")
        translate([0, center_plate_offset, 0])
        rotate([90,0,0])
        center_plate();
        
    color("grey")
        translate([full_x / 2 + thiskness, center_plate_offset - thiskness, 0])
        rotate([0,270,0])
        divider_center();
        
    color("orange")
        for(i = [1 : back_dividers_count])
            translate([ i * full_x / (back_dividers_count + 1), 0, 0])
            rotate([90,0,90])
            divider_back();

    color("brown")
        translate([full_x / 2, (full_y + center_plate_offset) / 2, 0])
        rotate([90,0,0])
        divider_left();
        
    color("lime")
        translate([full_x / 4 * 3, center_plate_offset + (full_y - center_plate_offset) / 2 - thiskness, 0])
        rotate([90,0,90])
        separator_left();

}

assembly();