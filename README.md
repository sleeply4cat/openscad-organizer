# OpenScad-Organizer

Simple organizer generator written in OpenSCAD. Pretty non-optimal and glitchy, but, hey, it was used in the real world.

![picure](https://bitbucket.org/sleeply4cat/openscad-organizer/raw/bc5ceb6507c9ae57db1fa894b4c6c438601c2202/screenshot.png)

You can edit parameters in the very first lines of script:

$fn - accuracy

thiskness - thiskness of used material
notch_step - size of joint notch
undercut_size - should be a bit bigger than radius of the mill
full_x, full_y, full_z - total dimensions 
front_z - height of th front plate
center_plate_offset - depth of the "pencil" section
back_dividers_count - count of "pencil" subsections. Should be odd.